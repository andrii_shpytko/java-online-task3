package com.epam.oopFlowershop.flower.flowerProperties;

public enum FlowerType {
    ROSE,
    TULIP,
    ORCHID,
    LILY,
    CHAMOMILE,
    VIOLET,
    CHRYSANTHEMUM,
    PEONY
}
