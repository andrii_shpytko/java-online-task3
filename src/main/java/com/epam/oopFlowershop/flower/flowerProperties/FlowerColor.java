package com.epam.oopFlowershop.flower.flowerProperties;

public enum FlowerColor {
    RED,
    YELLOW,
    WHITE,
    ROSE,
    GREEN

}
