package com.epam.oopFlowershop.flower;

import com.epam.oopFlowershop.flower.flowerProperties.FlowerColor;
import com.epam.oopFlowershop.flower.flowerProperties.FlowerType;

import java.time.LocalDateTime;

public class Flower {
    private FlowerType type;
    private FlowerColor color;
    private LocalDateTime manufactureDate;
    private double length;

    public Flower() {
    }

    public Flower(FlowerType type) {
        this.type = type;
    }

    public FlowerType getType() {
        return type;
    }

    public void setType(FlowerType type) {
        this.type = type;
    }

    public FlowerColor getColor() {
        return color;
    }

    public void setColor(FlowerColor color) {
        this.color = color;
    }

    public LocalDateTime getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(LocalDateTime manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "type=" + type +
                ", color=" + getColor() +
                ", manufactureDate=" + manufactureDate.getDayOfWeek() +
                ", length=" + length +
                '}';
    }
}
