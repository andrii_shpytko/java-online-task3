package com.epam.oopFlowershop;

import com.epam.oopFlowershop.accessory.Accessory;
import com.epam.oopFlowershop.accessory.AccessoryType;
import com.epam.oopFlowershop.bouquets.Bouquet;
import com.epam.oopFlowershop.flower.Flower;
import com.epam.oopFlowershop.flower.flowerProperties.FlowerType;
import com.epam.oopFlowershop.services.BouquetService;
import com.epam.oopFlowershop.services.PriceService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class App {

    private static PriceService priceService = new PriceService();
    private static BouquetService bouquetService = new BouquetService();
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private static final String SELECT_NUMBER_OPERATION = "Please choose an action by typing [0-6]";


    public static void main(String[] args) {
        List<Flower> myFlowers = new ArrayList<>();
        myFlowers.add(new Flower(FlowerType.ROSE));
        myFlowers.add(new Flower(FlowerType.ROSE));
        myFlowers.add(new Flower(FlowerType.ROSE));
        List<Accessory> myAccessory = new ArrayList<>();
        myAccessory.add(new Accessory(AccessoryType.FLOWER_WRAPPER));
        myAccessory.add(new Accessory(AccessoryType.DECORATION));

        Bouquet myBouquet = new Bouquet();
        bouquetService.addFlower(myBouquet, myFlowers);
        bouquetService.addAccessory(myBouquet, myAccessory);
        priceService.generatePrice(myBouquet);
//        Flower flower = bouquetService.getFlowerByLength(1.0);
//        List<Flower> sortedByLength = bouquetService.sortByManufacturingDate(myBouquet.getFlowers());
        Double myPrice = myBouquet.getPrice();
        System.out.println(myPrice);

//        System.out.println(sortedByLength.toString());
        System.out.println(myFlowers.toString());
    }
}
