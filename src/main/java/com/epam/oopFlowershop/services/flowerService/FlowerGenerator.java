package com.epam.oopFlowershop.services.flowerService;

import com.epam.oopFlowershop.flower.Flower;
import com.epam.oopFlowershop.flower.flowerProperties.FlowerColor;
import com.epam.oopFlowershop.flower.flowerProperties.FlowerType;

import java.time.LocalDateTime;
import java.util.HashMap;

public class FlowerGenerator {
    private static HashMap<FlowerType, Double> length = new HashMap<>();
    private static HashMap<FlowerType, Double> price = new HashMap<>();
    private static HashMap<FlowerType, LocalDateTime> manufacturingDate = new HashMap<>();
    private static HashMap<FlowerType, FlowerColor> color = new HashMap<>();

    static {
        length.put(FlowerType.CHAMOMILE, 80.0);
        length.put(FlowerType.CHRYSANTHEMUM, 75.0);
        length.put(FlowerType.ORCHID, 85.0);
        length.put(FlowerType.LILY, 55.0);
        length.put(FlowerType.PEONY, 45.0);
        length.put(FlowerType.ROSE, 60.0);
        length.put(FlowerType.TULIP, 40.0);
        length.put(FlowerType.VIOLET, 17.0);
    }

    static {
        price.put(FlowerType.CHAMOMILE, 20.0);
        price.put(FlowerType.CHRYSANTHEMUM, 28.0);
        price.put(FlowerType.ORCHID, 150.0);
        price.put(FlowerType.LILY, 30.0);
        price.put(FlowerType.PEONY, 45.0);
        price.put(FlowerType.ROSE, 40.0);
        price.put(FlowerType.TULIP, 25.0);
        price.put(FlowerType.VIOLET, 55.0);
    }
    static {
        manufacturingDate.put(FlowerType.CHAMOMILE, LocalDateTime.now().minusDays(2));
        manufacturingDate.put(FlowerType.CHRYSANTHEMUM, LocalDateTime.now().minusDays(3));
        manufacturingDate.put(FlowerType.ORCHID, LocalDateTime.now().minusDays(2));
        manufacturingDate.put(FlowerType.LILY, LocalDateTime.now().minusDays(4));
        manufacturingDate.put(FlowerType.PEONY, LocalDateTime.now().minusDays(1));
        manufacturingDate.put(FlowerType.ROSE, LocalDateTime.now());
        manufacturingDate.put(FlowerType.TULIP, LocalDateTime.now());
        manufacturingDate.put(FlowerType.VIOLET, LocalDateTime.now().minusDays(3));
    }

    static{
        color.put(FlowerType.CHAMOMILE, FlowerColor.WHITE);
        color.put(FlowerType.CHRYSANTHEMUM, FlowerColor.WHITE);
        color.put(FlowerType.ORCHID, FlowerColor.ROSE);
        color.put(FlowerType.LILY, FlowerColor.RED);
        color.put(FlowerType.PEONY, FlowerColor.WHITE);
        color.put(FlowerType.ROSE, FlowerColor.RED);
        color.put(FlowerType.TULIP, FlowerColor.WHITE);
        color.put(FlowerType.VIOLET, FlowerColor.WHITE);
    }

    public Double getPrice(FlowerType type) {
        return price.get(type);
    }

    public Double getLength(FlowerType type) {
        return length.get(type);
    }

    /**
     * assume that flowers of one type are imported in one day
     * @param type use type of flower
     * @return date of delivery of flowers of each type
     */
    public LocalDateTime getManufacturingDate(FlowerType type) {
        return manufacturingDate.get(type);
    }

    public FlowerColor gerColor(FlowerType type){
        return color.get(type);
    }

    public Flower getFlowerByLength(Double flowerLength){
        return (Flower) length
                .entrySet()
                .stream()
                .filter(entry -> flowerLength.equals(entry.getKey()))
                .findAny()
                .get();
    }

}
