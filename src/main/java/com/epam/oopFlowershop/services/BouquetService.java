package com.epam.oopFlowershop.services;

import com.epam.oopFlowershop.accessory.Accessory;
import com.epam.oopFlowershop.bouquets.Bouquet;
import com.epam.oopFlowershop.flower.Flower;
import com.epam.oopFlowershop.services.flowerService.FlowerGenerator;

import java.util.*;

public class BouquetService {
    private FlowerGenerator flowerGenerator = new FlowerGenerator();

    public void addFlower(Bouquet bouquet, List<Flower> flowers) {
        List<Flower> flowersList = new ArrayList<>(flowers);
        flowersList
                .stream()
                .forEach((Flower flower) -> flower.setLength(flowerGenerator.getLength(flower.getType())));
        flowersList
                .stream()
                .forEach((Flower flower) -> flower.setColor(flowerGenerator.gerColor(flower.getType())));
        flowersList
                .stream()
                .forEach((Flower flower) -> flower.setManufactureDate(flowerGenerator.getManufacturingDate(flower.getType())));
        bouquet.getFlowers().addAll(flowers);
    }

    public void addAccessory(Bouquet bouquet, List<Accessory> accessories) {
        bouquet.getAccessories().addAll(accessories);
    }

    public List<Flower> sortByManufacturingDate(List<Flower> flowers) {
        List<Flower> sortedFlowersList = new LinkedList<>();
        Collections.sort(flowers, new Comparator<Flower>() {
            @Override
            public int compare(Flower o1, Flower o2) {
                o1.getManufactureDate().isBefore(o2.getManufactureDate());
                return 1;
            }
        });
        return sortedFlowersList;
    }

    public Flower getFlowerByLength(double length){

        return flowerGenerator.getFlowerByLength(length);
    }
}
