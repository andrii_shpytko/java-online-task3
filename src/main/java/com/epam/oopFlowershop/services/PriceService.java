package com.epam.oopFlowershop.services;

import com.epam.oopFlowershop.accessory.Accessory;
import com.epam.oopFlowershop.bouquets.Bouquet;
import com.epam.oopFlowershop.flower.Flower;
import com.epam.oopFlowershop.services.accessoryService.AccessoryGenerator;
import com.epam.oopFlowershop.services.flowerService.FlowerGenerator;

public class PriceService {
    private FlowerGenerator flowerGenerator = new FlowerGenerator();
    private AccessoryGenerator accessoryGenerator = new AccessoryGenerator();

    /**
     * generate price of bouquet
     * @param bouquet
     */
    public void generatePrice(Bouquet bouquet){
        Double price = 0.0;
        for(Flower flower : bouquet.getFlowers()){
            price += flowerGenerator.getPrice(flower.getType());
        }

        for(Accessory accessory : bouquet.getAccessories()){
            price += accessoryGenerator.getPrice(accessory.getType());
        }
        bouquet.setPrice(price);
    }
}
