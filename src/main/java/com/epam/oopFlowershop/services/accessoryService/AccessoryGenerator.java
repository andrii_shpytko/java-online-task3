package com.epam.oopFlowershop.services.accessoryService;

import com.epam.oopFlowershop.accessory.AccessoryType;

import java.util.HashMap;

public class AccessoryGenerator {
    private static HashMap<AccessoryType, Double> prices = new HashMap<>();

    static {
        prices.put(AccessoryType.POT, 100.0);
        prices.put(AccessoryType.FLOWER_WRAPPER, 35.0);
        prices.put(AccessoryType.DECORATION, 55.0);
    }

    public Double getPrice(AccessoryType type) {
        return prices.get(type);
    }
}
