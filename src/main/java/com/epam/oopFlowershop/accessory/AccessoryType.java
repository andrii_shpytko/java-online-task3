package com.epam.oopFlowershop.accessory;

public enum AccessoryType {
    POT,
    FLOWER_WRAPPER,
    DECORATION;
}
