package com.epam.oopFlowershop.accessory;

public class Accessory {
    private AccessoryType type;

    public Accessory() {
    }

    public Accessory(AccessoryType type) {
        this.type = type;
    }

    public AccessoryType getType() {
        return type;
    }

    public void setType(AccessoryType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Accessory{" +
                "type=" + type +
                '}';
    }
}
