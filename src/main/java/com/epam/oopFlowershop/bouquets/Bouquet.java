package com.epam.oopFlowershop.bouquets;

import com.epam.oopFlowershop.accessory.Accessory;
import com.epam.oopFlowershop.flower.Flower;

import java.util.ArrayList;
import java.util.List;

/**
 * you can add flower через bouquetService
 */
public class Bouquet {

    private List<Flower> flowers = new ArrayList<>(); //you can add flower through bouquetService
    private List<Accessory> accessories = new ArrayList<>(); // you can add accessory through bouquetService
    private Double price; // you can generate price by priceService

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public List<Accessory> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Accessory> accessories) {
        this.accessories = accessories;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
