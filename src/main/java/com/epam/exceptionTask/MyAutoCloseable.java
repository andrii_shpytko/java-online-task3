package com.epam.exceptionTask;

public class MyAutoCloseable implements AutoCloseable{

    @Override
    public void close() throws Exception {
        System.out.println(" class MyAutoCloseable works!");
    }
}
