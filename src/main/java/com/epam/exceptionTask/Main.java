package com.epam.exceptionTask;

public class Main {
    public static void main(String[] args) throws MyException {

        try (MyAutoCloseable autoCloseable = new MyAutoCloseable()){

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
